<section>
        
    <form class="formLogin2" action="/user/register/request" method="POST">
        <fieldset>
            <legend><h1>Créer un compte</h1></legend>
            <div class="separator">

                <label for="lastname">Entrez votre nom</label>
                <input id="lastname"   type="text" name="lastname" required placeholder="Nom">     
                <label for="firstname">Entrez votre prénom</label>
                <input id="firstname"   name="firstname" type="text" required placeholder="Prénom">
                <label for="birthDate">Date de naissance</label>
                <input id="birthDate"  name="birthDate" type="date" required placeholder="Prénom">
                <label for="email">Entrez votre email</label>
                <input id="email" class="check"  id="emailInput" name="email" type="email" placeholder="example@test.com" required>
            </div>
            <hr>
            <div class="separator">
                <label for="username">Entrez un nom d'utilisateur</label>
                <input id="username" class="check" name="username" type="text" placeholder="Nom d'utilisateur" required>
                <label for="passw">Entrez un mot de passe</label>
                <input id="passw" class="check" name="passw" title="OBLIGATOIRE : Doit commencer par une majuscule et contenir des chiffres"  type="password" placeholder="8 caractères mini." required>
                <label for="confirmPassw">Confirmer votre mot de passe</label>
                <input id='confirmPassw'  name="confirmPassw" type="password" placeholder="mot de passe" required> 
            </div>
            <button type="submit">Se connecter</button>
            <p>Vous avez un compte ? <a href="/user/">Connectez-vous</a></p>
            </fieldset>            
    </form>
</section>
<style>
header {
  box-shadow: 0px 1px 10px 0px grey;
}
</style>


<script src="/js/formChecker.js"></script>