<style>
	header {
	  box-shadow: 0px 1px 10px 0px grey;
	}


</style>
<div id="carousel" class="carouselContainer">
	
</div>
<div class="textContainer">
	<h1 class="homeTitle">Boutique en ligne</h1>
	<p class="homePara">Découvrez nos vêtements, toujours au goût du jour. Nous proposons des vêtements destiné aux femmes, aux hommes et aux enfants ! </p>
</div>
<section>
	<div class="categoriesContainer">
		<h2>Choisissez une catégorie</h2>
		<?php foreach ($context['app']['categories'] as $category) {?> 
			<a href="/categorie/<?= $category->getId()?>-<?= ucfirst($category->getName())?>">
				<div>
					<p><?= ucfirst($category->getName()) ?></p>
				</div>
			</a>


		<?php } ?>
	</div>
</section>


<script src="/js/carousel.js"></script>

