<footer>
    <div class="footerContainer">
        <div class="leftSide">
            <div class="footerRubriqueLeft">
                <p class="fontW">INFORMATIONS SUR L'ENTREPRISE</p>
                <a class="linkStyleDisabled" href="">Qui Sommes-nous?</a>
                <a class="linkStyleDisabled" href="">Affilié</a>
                <a class="linkStyleDisabled" href="">Blogger</a>
                <a class="linkStyleDisabled" href="">Responsabilité Sociale</a>
              
                <span>Changer le thème</span>
                <label class="switchContainer">
                    <input id="themeSlider" type="checkbox">
                    <span class="slider round"></span>
                </label>
            </div>
            <div class="footerRubriqueLeft">
                <p class="fontW">AIDE</p>
                <a class="linkStyleDisabled" href="">Livraison</a>
                <a class="linkStyleDisabled" href="">Retour</a>
                <a class="linkStyleDisabled" href="">Commande</a>
                <a class="linkStyleDisabled" href="">Statut de commande</a>
                <a class="linkStyleDisabled" href="">Guide des tailles</a>
                <a class="linkStyleDisabled" href="">Be FAMUS VIP</a>
            </div>
            <div class="footerRubriqueLeft">
                <p class="fontW">CONTACT & PAIEMENT</p>
                <a class="linkStyleDisabled" href="">Nous contacter</a>
                <a class="linkStyleDisabled" href="">Paiements et Taxes</a>
                <a class="linkStyleDisabled" href="">Points bonus</a>
                <a class="linkStyleDisabled" href="">Clearpay</a>
                <a class="linkStyleDisabled" href="">Klarna</a>
                <a class="linkStyleDisabled" href="">Scalapay</a>
                <a class="linkStyleDisabled" href="">Avis de rappel</a>
            </div>
        </div>
        <div class="rightSide">
                <div class="footerRubriqueRight">
                    <p>Trouvez-nous sur</p>
                    <div>
                        <a href=""><i class="bi bi-facebook"></i></a>
                        <a href=""><i class="bi bi-youtube"></i></a>
                        <a href=""><i class="bi bi-instagram"></i></a>
                        <a href=""><i class="bi bi-snapchat"></i></a>
                        <a href=""><i class="bi bi-pinterest"></i></a>
                    </div>
                </div>
        </div>
    </div>

</footer>
		
<script src="/js/script.js"></script>