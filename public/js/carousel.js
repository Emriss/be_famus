class Carousel {

    constructor(parent) {
        this._parent = parent
        this._i = 0;
        this._imgArray = [
            '/images/carousel/carousel1.jpg',
            '/images/carousel/carousel2.jpg',
            '/images/carousel/carousel3.jpg',
            '/images/carousel/carousel4.jpg',
        ]

        const container = document.createElement('div');
        container.classList.add('carousel');

        const prevButton = document.createElement('button');
        prevButton.classList.add('carouselBtn', 'prevBtn');
        const prevIcon = document.createElement('I');
        prevIcon.classList.add('bi','bi-caret-left');
        prevButton.appendChild(prevIcon);
        container.appendChild(prevButton);
        
      

        const nextButton = document.createElement('button');
        nextButton.classList.add('carouselBtn', 'nextBtn');
        const nextIcon = document.createElement('I');
        nextIcon.classList.add('bi','bi-caret-right');
        nextButton.appendChild(nextIcon);
        container.appendChild(nextButton);


        prevButton.addEventListener('click', () => {
            this.prevImg(imgDisplay)
        })
        nextButton.addEventListener('click', () => {
            this.nextImg(imgDisplay)
        })

        const imgDisplay = document.createElement('img');
        imgDisplay.src = this._imgArray[this._i]
        container.appendChild(imgDisplay);
        this._parent.appendChild(container);
        setInterval(() => {
            this._i < this._imgArray.length - 1 ? this._i++ : this._i = 0;
            imgDisplay.src = this._imgArray[this._i]
        },4000)
    }

    nextImg(imgDisplay) {
        this._i < this._imgArray.length - 1 ? this._i++ : this._i = 0;
        imgDisplay.src = this._imgArray[this._i];
    }
    prevImg(imgDisplay) {
        this._i > 0 ? this._i-- : this._i = this._imgArray.length - 1;
        imgDisplay.src = this._imgArray[this._i];
    }
}

window.addEventListener('load', () => {
    let carousel = new Carousel(document.getElementById('carousel'));
})